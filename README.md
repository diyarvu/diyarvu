# DIYARvu
… is a project that aims to create a head mounted augmented reality display.

## How does it work?
This repository contains the code for a python/panda3d application that can be run on a raspberry pi or similar small computer.

> Be careful when selecting your operating system for the raspberry pi, since panda3d only runs on 64-bit OSes.

DIYARvu uses stereo vision to provide an image for each eye so your brain interprets the images as 3d.

## What is the goal of this project?
This project is the basis for many applications which include:
- [ ] Extend your desktop or laptop with virtual screens for a more convenient and private mobile office.
- [ ] Play virtual reality games.

and many other things.