from typing import List

from direct.showbase.ShowBase import ShowBase

from diyarvu.plugins import BasePlugin, AppPlugin, TaskPlugin, PreInitConfig


class Headset(ShowBase):

    def __init__(self,
                 plugin_list: List,
                 ):
        """
        This class is the actual panda3d app that makes up the core part of the software.

        :param plugin_list: List of plugins.
        """

        for plugin in plugin_list:
            if issubclass(plugin.__class__, PreInitConfig):
                plugin()

        ShowBase.__init__(self)

        # Initialize plugins
        self.plugins: List[AppPlugin] = list()
        for plugin in plugin_list:
            if issubclass(plugin.__class__, AppPlugin):
                plugin.set_app(self)
                plugin()
            self.plugins.append(plugin)

        for plugin in self.plugins:
            if issubclass(plugin.__class__, AppPlugin):
                plugin: AppPlugin
                plugin.__call__()

        for plugin in self.plugins:
            if issubclass(plugin.__class__, TaskPlugin):
                plugin: TaskPlugin
                self.taskMgr.add(plugin.run, str(plugin))
