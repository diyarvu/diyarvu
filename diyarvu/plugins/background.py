from diyarvu.plugins import AppPlugin


class Color(AppPlugin):
    def __init__(self, r, g, b):
        """
        Add custom background colors
        :param r: red in 0 - 1
        :param g: green in 0 - 1
        :param b: blue in 0 - 1
        """
        self.r, self.g, self.b = r, g, b

    def __call__(self, *args, **kwargs):
        self.app.setBackgroundColor(self.r, self.g, self.b)


class Black(Color):
    def __init__(self):
        super(Black, self).__init__(0, 0, 0)


class White(Color):
    def __init__(self):
        super(White, self).__init__(1, 1, 1)
