import numpy as np

from direct.task import Task

from diyarvu.plugins import TaskPlugin, AppPlugin


class Locator(AppPlugin):
    """
    Sets an initial position for the camera
    For debugging purposes only
    """

    def __call__(self, *args, **kwargs):
        self.app.camera.setPos(0, -20, 1)


class Keyboard(TaskPlugin):
    def __init__(self):
        super(Keyboard, self).__init__()
        self.keys = None
        self.keyMap = None
        self.set_key_map()

        self._position = np.zeros(shape=3)
        self._rotation = np.zeros(shape=3)
        self._last_executed = 0.0

    def set_key_map(self,
                    forwards: str = "w",
                    left: str = "a",
                    backwards: str = "s",
                    right: str = "d",
                    turn_left: str = "arrow_left",
                    turn_right: str = "arrow_right",
                    turn_up: str = "arrow_up",
                    turn_down: str = "arrow_down",
                    vertical_up: str = "space",
                    vertical_down: str = "shift",
                    ):
        self.keys = [
            forwards,
            left,
            backwards,
            right,
            turn_left,
            turn_right,
            turn_up,
            turn_down,
            vertical_up,
            vertical_down,
        ]
        self.keyMap = {k: False for k in self.keys}

    def __call__(self):
        for k in [k for k in self.keys]:
            self.app.accept(k, self.update_map, [k, True])
            self.app.accept(k + "-up", self.update_map, [k, False])

    def update_map(self, key, state):
        self.keyMap[key] = state

    def get_key(self, key: [int, str]) -> bool:
        """
        Return the boolean behind the key either by index or by str
        :param key: [int, str]
        :return: bool
        """
        if type(key) == int:
            return self.get_key(self.keys[key])
        return self.keyMap[key]

    def update_position(self, duration: float):
        # TODO get data from actual camera to allow for external changes
        t = np.deg2rad(self._rotation[0])
        pos = np.array([
            self.get_key(3) * 1 + self.get_key(1) * -1,
            self.get_key(0) * 1 + self.get_key(2) * -1,
            self.get_key(8) * 1 + self.get_key(9) * -1,
        ]).dot([
            [np.cos(t), np.sin(t), 0],
            [-np.sin(t), np.cos(t), 0],
            [0, 0, 1],
        ]) * duration * 10
        self._position += pos
        self.app.camera.setPos(*self._position)

    def update_rotation(self, duration: float):
        # TODO get data from actual camera to allow for external changes
        trans = np.array([
            self.get_key(4) * 1 + self.get_key(5) * -1,
            self.get_key(6) * 1 + self.get_key(7) * -1,
            0
        ]) * duration * 45
        self._rotation += trans
        self.app.camera.setHpr(*self._rotation)

    def run(self, task: Task):
        self.update_rotation(task.time - self._last_executed)
        self.update_position(task.time - self._last_executed)
        self._last_executed = task.time
        return Task.cont
