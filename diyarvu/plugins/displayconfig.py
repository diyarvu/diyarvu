from typing import List


from panda3d.core import ConfigVariable, ConfigVariableInt, WindowProperties


from diyarvu.plugins import PreInitConfig, AppPlugin


# use a custom screen size
class SetScreenSize(PreInitConfig):
    variable_name = "win-size"

    @staticmethod
    def by_pixels(width: int, height: int):
        return SetScreenSize(size=[width, height])

    @staticmethod
    def by_pixel_width_and_aspect_ratio(width: int, aspect_ratio: float):
        return SetScreenSize.by_pixels(width=width, height=round(width / aspect_ratio))

    @staticmethod
    def by_real_size(width_mm, height_mm, dpi: int):
        width_px = round(width_mm * dpi / 25.4)
        height_px = round(height_mm * dpi / 25.4)
        return SetScreenSize.by_pixels(width_px, height_px)

    @staticmethod
    def by_real_width_and_aspect_ratio(width_mm, aspect_ratio, dpi):
        return SetScreenSize.by_real_size(width_mm=width_mm, height_mm=width_mm / aspect_ratio, dpi=dpi)

    def __init__(self, size: List = None):
        self.value = " ".join([str(v) for v in size])
        super(SetScreenSize, self).__init__()


class EnableFullscreen(PreInitConfig):
    variable_name = "fullscreen"
    value = True


# Side by side stereo 3d
class EnableVR(PreInitConfig):
    variable_name = "side-by-side-stereo"
    value = 1
    type_ = ConfigVariableInt


# Vertical Mirroring and stereo 3d
class EnableAR(PreInitConfig):
    variable_name = "window-inverted"
    value = 1
    type_ = ConfigVariable

    def __init__(self):
        super(EnableAR, self).__init__()
        # Enabling AR requires enabling vr and inverting the screen
        self.enable_vr = EnableVR()
        self.enable_vr()
        super(EnableAR, self).__init__()


class HideCursor(AppPlugin):
    def __call__(self, *args, **kwargs):
        props = WindowProperties()
        props.setCursorHidden(True)
        self.app.win.requestProperties(props)
