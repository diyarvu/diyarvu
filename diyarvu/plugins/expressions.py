import numpy as np
from direct.task import Task
from panda3d.core import CardMaker, Texture

from diyarvu.plugins import AppPlugin


class ShowNumpyArray(AppPlugin):
    def __init__(self):
        super(ShowNumpyArray, self).__init__()
        self.image = None
        self.tex = None
        self.callback = None
        self.frame = np.array((720, 640, 3)).astype(np.uint8)

    def __call__(self, *args, **kwargs):
        cm = CardMaker("card")
        self.image = self.app.render.attachNewNode(cm.generate())
        self.image.setScale(16 / 4, 0, 9 / 4)
        self.image.setPos(-16 / 4, 0, 2)
        # self.tex.setCompression(Texture.CMOff)

    def set_callback(self, callback):
        self.callback = callback

    def update_texture(self):
        h, w, d = self.frame.shape
        self.tex = Texture()
        self.tex.setup2dTexture(h, w, Texture.T_unsigned_byte, Texture.F_luminance)

    def get_frame(self):
        if self.frame is not None:
            old_shape = self.frame.shape
        else:
            old_shape = None
        if self.callback is not None:
            self.frame = self.callback()
        else:
            self.frame = np.zeros((720, 640, 3)).astype(np.uint8)
        if old_shape != self.frame.shape or self.tex is None:
            self.update_texture()
        return self.frame is not None, self.frame

    def run(self, task: Task):
        success, frame = self.get_frame()
        if success:
            buf = frame[:, :, 0].T.tostring()  # slice RGB to gray scale, transpose 90 degree, convert to text buffer
            self.tex.setRamImage(buf)  # overwriting the memory with new buffer
            self.image.setTexture(self.tex)
        return task.cont
