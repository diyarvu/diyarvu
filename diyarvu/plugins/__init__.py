from direct.task import Task
from direct.showbase import ShowBase
from panda3d.core import ConfigVariable


class BasePlugin:
    def __str__(self):
        return self.__class__.__name__


class AppPlugin(BasePlugin):
    def __init__(self):
        super(AppPlugin, self).__init__()
        self.app: ShowBase = None

    def set_app(self, app: ShowBase):
        self.app = app


class PreInitConfig(BasePlugin):
    variable_name = None
    value = None
    type_ = ConfigVariable

    def __call__(self, *args, **kwargs):
        self.config_variable = self.type_(self.variable_name)
        self.config_variable.setStringValue(str(self.value))


class TaskPlugin(AppPlugin):
    def __call__(self, *args, **kwargs):
        return None

    def run(self, task: Task) -> Task.cont:
        return Task.cont
