from math import sin, cos, pi

from direct.task import Task

from diyarvu.plugins import TaskPlugin, AppPlugin


class LoadBasicSceneryDemo(AppPlugin):
    def __call__(self, *args, **kwargs):
        # Load the environment model.
        self.app.scene = self.app.loader.loadModel("models/environment")
        # Reparent the model to render.
        self.app.scene.reparentTo(self.app.render)
        # Apply scale and position transforms on the model.
        self.app.scene.setScale(0.25, 0.25, 0.25)
        self.app.scene.setPos(-8, 42, 0)


class SpinCameraDemo(TaskPlugin):
    """
    This demo class is the equivalent to the native panda3d example on how to control the camera.
    https://docs.panda3d.org/1.10/python/introduction/tutorial/controlling-the-camera
    """

    def run(self, task: Task) -> Task.cont:
        angle_degrees = task.time * 6.0
        angle_radians = angle_degrees * (pi / 180.0)
        self.app.camera.setPos(20 * sin(angle_radians), -20 * cos(angle_radians), 3)
        self.app.camera.setHpr(angle_degrees, 0, 0)
        return Task.cont
