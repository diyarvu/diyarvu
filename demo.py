import sys


from diyarvu.plugins import background
from diyarvu.plugins.displayconfig import EnableVR, SetScreenSize
from diyarvu.plugins.demo import LoadBasicSceneryDemo, SpinCameraDemo
from diyarvu import Headset


app = Headset(
    plugin_list=[
        EnableVR(),
        SetScreenSize.by_pixels(1280, 720),
        background.Black(),
        LoadBasicSceneryDemo(),
        SpinCameraDemo(),
    ]
)
app.accept("escape", sys.exit)
app.run()
