import sys

from diyarvu.plugins import background, demo, displayconfig, expressions, movement
from diyarvu import Headset


app = Headset(
    plugin_list=[
        displayconfig.EnableAR(),
        displayconfig.EnableFullscreen(),
        displayconfig.HideCursor(),
        movement.Keyboard(),
        background.Black(),
        demo.LoadBasicSceneryDemo(),
        expressions.ShowNumpyArray(),
    ]
)
app.accept("escape", sys.exit)
app.run()
